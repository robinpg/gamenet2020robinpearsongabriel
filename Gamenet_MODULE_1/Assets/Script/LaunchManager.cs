﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LaunchManager : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;
    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    private void Start()
    {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
       
       
    }
    public override void OnJoinRandomFailed(short returnCode, string message)//4.5
    {
        Debug.LogWarning("no room");
        CreateAndJoinRoom();
    }
    public override void OnConnectedToMaster()//3
    {
        Debug.Log(PhotonNetwork.NickName+" connected to photon server");
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
    }

    public override void OnConnected()//1
    {
        Debug.Log("Connected to the internet");
    }
    public void ConnectToPhotonServer()//2
    {
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
        EnterGamePanel.SetActive(false);
        ConnectionStatusPanel.SetActive(true);
    }
    public void JoinRandomRoom()//4
    {
        PhotonNetwork.JoinRandomRoom();
    }
    private void CreateAndJoinRoom()//5
    {
        string randomRoomName = "Room" + Random.Range(0, 10000);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;

        roomOptions.MaxPlayers = 20;
        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
    }
    public override void OnJoinedRoom()//6
    {
        Debug.Log(PhotonNetwork.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + " has entered the room " + PhotonNetwork.CurrentRoom.Name + " Room has now " + PhotonNetwork.CurrentRoom.PlayerCount +" players");
    }


}
