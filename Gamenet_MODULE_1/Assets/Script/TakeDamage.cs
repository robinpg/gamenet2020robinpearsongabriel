﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthbar;
    private float startHealth = 100;
    public float health = 100;
    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
    }

    private void Update()
    {
        healthbar.fillAmount = health / startHealth;
    }
    [PunRPC]
    // Update is called once per frame
    public void Takedamage(int damage)
    {
        health -= damage;
        Debug.Log(health);
        if (health <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
        if(photonView.IsMine)
        GameManager.instance.LeaveRoom();
    }
}
